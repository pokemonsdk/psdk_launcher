# File that loads launcher dependencies
# © 2021 Nuri Yuri
begin
  ENV['SSL_CERT_FILE'] = File.join(File.expand_path('.'), 'lib/cert.pem')
  ENV['__GL_THREADED_OPTIMIZATIONS'] = '0'
  require_relative 'lib/LiteRGSS.so'
  require 'zlib'
  require 'digest'
  require 'base64'
  require 'socket'
  require 'uri'
  require 'openssl'
  require 'net/http'
  require 'csv'
  require 'json'
  require './pokemonsdk/scripts/ScriptLoad'
  # require 'yaml'
  # require 'rexml/document'
rescue LoadError
  puts "\e[31m#{$!.message}\e[37m"
  Process.exit!
end

class Binding
  alias [] local_variable_get
  alias []= local_variable_set
end
