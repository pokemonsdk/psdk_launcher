class GlobalUI
  def initialize
    create_window
    @viewport = LiteRGSS::Viewport.new(@window, 0, 0, 800, 600)
    @helper = Helper.new
    create_stack
    self.state = :frozen
    create_hooks
  end

  def start
    try_to_connect
    @helper.make_game_folders
    test_new_version if state == :new_version_search
    loop do
      @window.update
      perform_non_frozen_step unless PENDING_STATES.include?(state)
    end
  end

  private

  def create_stack
    stack = @stack = SpriteStack.new(@viewport)
    stack.add_sprite(0, 0, 'background')
    @game_text = stack.add_text(30, 30, 0, 18, 'Pokémon SDK Launcher')
    @version_text = stack.add_text(@viewport.rect.width - 30, 30, 0, 18, '', 2)
    @error_text = stack.add_text(30, 422, 0, 18, '', color: 3)
    stack.add_sprite(355, 214, 'pw_logo')
    stack.add_sprite(306, 323, 'psdk_name')
    # @type [Button]
    @start_button = stack.add_sprite(641, 538, nil, 'Start', :base, type: Button)
    # @type [Button]
    @download_button = stack.add_sprite(493, 538, nil, 'Download', :disabled, type: Button)
    # @type [DownloadBar]
    @download_bar = stack.add_sprite(29, 546, nil, type: DownloadBar)
    create_additional_buttons(stack)
  end

  def create_additional_buttons(stack)
    # @type [Button]
    @start3_button = stack.add_sprite(641, 486, nil, 'Start x3', :disabled, type: Button)
    # @type [Button]
    @start6_button = stack.add_sprite(493, 486, nil, 'Start x6', :disabled, type: Button)
    # @type [Button]
    @startff_button = stack.add_sprite(345, 486, nil, 'Full Screen', :disabled, type: Button)
    # @type [Button]
    @system_tag_button = stack.add_sprite(641, 434, nil, 'System Tags', :disabled, type: Button)
    # @type [Button]
    @town_map_button = stack.add_sprite(493, 434, nil, 'Town Map', :disabled, type: Button)
    # @type [Button]
    @upd_data_button = stack.add_sprite(345, 434, nil, 'Update Data', :disabled, type: Button)
  end

  def try_to_connect
    @helper.load_config
    @version_text.text = @helper.game_version_text
    lvs = @helper.test_connection
    self.state = :connecting
    t = Time.new
    until lvs[:connected] && !lvs[:thread].status
      @window.update
      @download_bar.progress = (Time.new - t) / 2.0
    end
    result = lvs[:connected]
    @error_text.text = result if result.is_a?(String)
    self.state = result == true ? :new_version_search : :frozen
  end

  def test_new_version
    lvs = @helper.test_new_version
    t = Time.new
    while lvs[:thread].status
      @window.update
      @download_bar.progress = (Time.new - t) / 2.0
    end
    if lvs[:new_version]
      self.state = :download_allowed
      @version_file = lvs[:version]
    else
      self.state = @helper.can_be_played? ? :play_allowed : :frozen
    end
  end
end
