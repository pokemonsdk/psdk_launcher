class DownloadBar < SpriteStack
  LEGAL_STATES = %i[checking connecting new_version_search start download verification]
  # Get the bar progress (0~1)
  # @return [Float]
  attr_reader :progress

  # Get the state of the progress bar
  # @return [Symbol]
  attr_reader :state

  # Create a new bar
  # @param viewport [LiteRGSS::Viewport]
  def initialize(viewport)
    super
    @bg = add_sprite(0, 0, 'download_bar_bg')
    @front = add_sprite(1, 0, 'download_bar')
    @progress_text = add_text(0, -33, 0, 18, '', color: 2)
    @progress = 0
    @state = LEGAL_STATES.first
  end

  def progress=(v)
    @progress = v.clamp(0, 1)
    @front.src_rect.width = @front.bitmap.width * @progress
    update_text(@progress * 100)
  end

  def state=(state)
    return unless LEGAL_STATES.include?(state)

    @state = state
    self.progress = progress
  end

  private

  def update_text(percent)
    case state
    when :connecting
      @progress_text.text = format('Connecting in progress... %<percent>.2f%%', percent: percent)
    when :checking
      @progress_text.text = format('Check in progress... %<percent>.2f%%', percent: percent)
    when :verification
      @progress_text.text = format('Verification in progress... %<percent>.2f%%', percent: percent)
    when :new_version_search
      @progress_text.text = format('Searching new version... %<percent>.2f%%', percent: percent)
    when :start
      @progress_text.text = format('Starting game... %<percent>.2f%%', percent: percent)
    when :download
      @progress_text.text = format('Downloading... %<percent>.2f%%', percent: percent)
    else
      @progress_text.text = ''
    end
  end
end
