
# Class that helps to define a single object constitued of various sprites.
# With this class you can move the sprites as a single sprite, change the data that generate the sprites and some other cool stuff
class SpriteStack
  # X coordinate of the sprite stack
  # @return [Numeric]
  attr_reader :x
  # Y coordinate of the sprite stack
  # @return [Numeric]
  attr_reader :y
  # Data used by the sprites of the sprite stack to generate themself
  attr_reader :data
  # Get the stack
  attr_reader :stack
  # Get the viewport
  # @return [Viewport]
  attr_reader :viewport

  # Create a new Sprite stack
  # @param viewport [Viewport] the viewport where the sprites will be shown
  def initialize(viewport)
    @viewport = viewport
    @stack = []
    @x = 0
    @y = 0
  end

  # Push a sprite to the stack
  # @param x [Numeric] the relative x position of the sprite in the stack (sprite.x = stack.x + x)
  # @param y [Numeric] the relative y position of the sprite in the stack (sprite.y = stack.y + y)
  # @param args [Array] the arguments after the viewport argument of the sprite to create the sprite
  # @param type [Class] the class to use to generate the sprite
  # @return [LiteRGSS::Sprite] the pushed sprite
  def push(x, y, bmp, *args, type: LiteRGSS::Sprite)
    sprite = type.new(@viewport, *args)
    sprite.set_position(@x + x, @y + y)
    sprite.bitmap = LiteRGSS::Bitmap.new(TEXTURES[bmp], true) if TEXTURES[bmp]
    @stack << sprite
    return sprite
  end
  alias add_sprite push

  # Add a text inside the stack, the offset x/y will be adjusted
  # @param x [Integer] the x coordinate of the text surface
  # @param y [Integer] the y coordinate of the text surface
  # @param width [Integer] the width of the text surface
  # @param height [Integer, nil] the height of the text surface (if nil, uses the line_height from sizeid)
  # @param str [String] the text shown by this object
  # @param align [0, 1, 2] the align of the text in its surface (best effort => no resize), 0 = left, 1 = center, 2 = right
  # @param outlinesize [Integer, nil] the size of the text outline
  # @param type [Class] the type of text
  # @param color [Integer] the id of the color
  # @return [LiteRGSS::Text] the text object
  def add_text(x, y, width, height, str, align = 0, outlinesize = 0, type: LiteRGSS::Text, color: nil, sizeid: nil)
    height ||= 18
    text = type.new(0, @viewport, x + @x, y + @y, width, height, str, align, outlinesize, color, sizeid)
    text.draw_shadow = false
    @stack << text
    return text
  end

  # Change the x and y coordinate of the sprite stack
  # @param x [Numeric] the new x value
  # @param y [Numeric] the new y value
  # @return [self]
  def set_position(x, y)
    delta_x = x - @x
    delta_y = y - @y
    return move(delta_x, delta_y)
  end

  # Move the sprite stack
  # @param delta_x [Numeric] number of pixel the sprite stack should be moved in x
  # @param delta_y [Numeric] number of pixel the sprite stack should be moved in y
  # @return [self]
  def move(delta_x, delta_y)
    @x += delta_x
    @y += delta_y
    @stack.each { |sprite| sprite.set_position(sprite.x + delta_x, sprite.y + delta_y) }
    return self
  end

  # If the sprite stack is visible
  # @note Return the visible property of the first sprite
  # @return [Boolean]
  def visible
    return false if @stack.empty?
    return @stack.first.visible
  end

  # Change the visible property of each sprites
  # @param value [Boolean]
  def visible=(value)
    @stack.each { |sprite| sprite.visible = value }
  end
end
