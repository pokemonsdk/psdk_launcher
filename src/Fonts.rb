LiteRGSS::Fonts.module_eval do
  col = LiteRGSS::Color
  load_font(0, 'C:/Windows/Fonts/calibri.ttf')
  set_default_size(0, 18)
  [col.new(64, 141, 191), col.new(57, 59, 67), col.new(255, 255, 255), col.new(204, 94, 94)].each_with_index do |c, i|
    define_fill_color(i, c)
    define_shadow_color(i, c)
    define_outline_color(i, c)
  end
end
