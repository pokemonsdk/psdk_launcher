# Class responsive of digesting data (hashing) over several CPU core using Ractors
#
# How to use ?
#   1. First of all encapsulate all your data into IO object (File or StringIO)
#   2. Then create a new IODigester object by passing the Digest class you need and all the IO to digest in a Hash
#   3. Call the start function to let the system instanciate everything
#     * You can check the overall progress using #progess, it's a number between 0 & 1
#   4. If failed? respond true, you can grab the failure info using #failures (key = IO key, value = StandardError object)
#
# @example Demo
#   digester = IODigester.new(Digest::SHA1, { 'test' => io1, 'test2' => io2 })
#   digester.start { |d| print format("\r%<progress>.2f%%", progress: d.progress * 100) }
#   digester.hashes # => { 'test' => '<SHA1 of test>', 'test2' => '<SHA1 of test2>' }
#
# @note IODigester requires the IO to respond to size and give accurate value for size (in bytes)!
class IODigester
  # Get the hashes
  # @return [Hash{ String => String }]
  attr_reader :hashes
  # Get the failures
  # @return [Hash{ String => StandardError }]
  attr_reader :failures

  # Create a new IODigester
  # @param klass [Class<Digest::Base>] digest class to use to digest data
  # @param ios [Hash{ String => IO }] io to digest (key can be symbol if you want)
  def initialize(klass, ios)
    @klass = klass
    @ios = ios.clone
    @progresses = @ios.keys.map { |k| [k, 0] }.to_h
    @hashes = {}
    @failures = {}
  end

  # Start the process
  # @return [self]
  # @yieldparam d [self] current digester
  def start(&block)
    raise 'Already started' if @ractors

    @ractors = @ios.map { |key, value| make_ractor(key, value) }
    process_ractors(block)
    return self
  end

  # Test if the process has failures
  # @return [Boolean]
  def failed?
    return !@failures.empty?
  end

  # Get the progress
  # @return [Float]
  def progress
    @progresses.reduce(0.0) { |prev, curr| prev + curr.last } / @progresses.size
  end

  private

  # Function that processes all the ractors (in a Thread in order to not lock the GVL)
  # @param block [Proc]
  def process_ractors(block)
    until @ractors.empty?
      begin
        ractor, object = Ractor.select(*@ractors)
        parse_ractor_response(ractor, object)
        block&.call(self)
      rescue Ractor::RemoteError => e
        @failures[e.ractor.name] = e.cause
        @ractors.delete(e.ractor)
      end
    end
  end

  # Function that analyzes the payload of the received object from Ractor.select
  # @param ractor [Ractor] ractor object that we're talking about
  # @param object [Array] response from the ractor
  def parse_ractor_response(ractor, object)
    message, body = object
    case message
    when :progress
      @progresses[ractor.name] = body
    when :result
      @progresses[ractor.name] = 1
      @hashes[ractor.name] = body
      @ractors.delete(ractor)
    end
  end

  # Function that creates the ractor
  # @param name [String] name of the io (hash key)
  # @param io [IO] io to process
  # @return [Ractor]
  def make_ractor(name, io)
    ractor = Ractor.new(@klass, io, name: name) do |digest_klass, io_to_process|
      close_incoming
      # @type [Integer]
      io_size = io_to_process.size
      io_read = 0
      # @type [Digest::Base]
      digest = digest_klass.new
      while io_size > io_read
        Ractor.yield [:progress, io_read.to_f / io_size]
        # @type [String]
        data = io_to_process.read(20_971_520)
        io_read += data.bytesize
        digest << data
      end
      Ractor.yield [:result, digest.hexdigest]
    end
    return ractor
  end
end
