class GlobalUI
  LEGAL_STATES = %i[frozen connecting new_version_search download_allowed play_allowed start download]
  PENDING_STATES = %i[frozen play_allowed download_allowed]

  # Get the current state
  # @return [Symbol]
  attr_reader :state

  # Set the current state
  # @param state [Symbol]
  def state=(state)
    return unless LEGAL_STATES.include?(state)

    @state = state
    puts "New state: #{state}"
    send(:"set_#{state}_state")
  end

  private

  # Set the UI in frozen state
  def set_frozen_state
    @error_text.visible = @game_text.visible = @version_text.visible = true
    @download_button.visible = @download_bar.visible = false
    @start_button.state = :disabled
  end

  # Set the UI in connecting state
  def set_connecting_state
    @error_text.visible = @download_button.visible = false
    @game_text.visible = @version_text.visible = @download_bar.visible = true
    @start_button.state = :disabled
    @download_bar.state = :connecting
  end

  # Set the UI in connecting state
  def set_new_version_search_state
    @error_text.visible = @download_button.visible = false
    @game_text.visible = @version_text.visible = @download_bar.visible = true
    @start_button.state = :disabled
    @download_bar.state = :new_version_search
  end

  # Set the UI in connecting state
  def set_download_allowed_state
    @download_bar.visible = @error_text.visible = false
    @game_text.visible = @version_text.visible = @download_button.visible = true
    @start_button.state = :disabled
    @download_button.state = :base
    @download_button.on_click = proc { self.state = :download }
  end

  UPDATE_COMMAND = %q{ruby -e"File.delete('Data/PSDK/ItemData.25.rxdata');system('psdk.bat --util=update_db_symbol')"}
  # Set the UI in connecting state
  def set_play_allowed_state
    @download_bar.visible = @error_text.visible = @download_button.visible = false
    @game_text.visible = @version_text.visible = true
    @start_button.state = :base
    @start3_button.state = @start6_button.state = @startff_button.state = :base
    @system_tag_button.state = @town_map_button.state = @upd_data_button.state = :base
    @start_button.on_click = proc { system('psdk.bat debug --scale=2') }
    @start3_button.on_click = proc { system('psdk.bat debug --scale=3') }
    @start6_button.on_click = proc { system('psdk.bat debug --scale=6') }
    @startff_button.on_click = proc { system('psdk.bat debug --fullscreen') }
    @system_tag_button.on_click = proc { system('psdk.bat --tags') }
    @town_map_button.on_click = proc { system('psdk.bat --worldmap') }
    @upd_data_button.on_click = proc { system(UPDATE_COMMAND) }
  end

  def system(str)
    Thread.new do
      Kernel.system(str)
    end
  end

  # Set the UI in connecting state
  def set_start_state
    @error_text.visible = @download_button.visible = false
    @game_text.visible = @version_text.visible = @download_bar.visible = true
    @start_button.state = :disabled
    @download_bar.state = :start
  end

  # Set the UI in connecting state
  def set_download_state
    @error_text.visible = @download_button.visible = false
    @game_text.visible = @version_text.visible = @download_bar.visible = true
    @start_button.state = :disabled
    @download_bar.state = :checking
  end
end
