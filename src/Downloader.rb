class Downloader
  # Get the error
  # @return [String, nil]
  attr_reader :error

  # @param files_to_download [Array<String>]
  # @param game_url [String]
  # @param game_version [String]
  def initialize(files_to_download, game_url, game_version)
    @files_to_download = files_to_download
    @game_url = game_url
    @game_version = game_version
  end

  def start(&block)
    thread = create_download_thread
    block.call(thread[:progress] || 0) while thread.status
    if thread.status.nil?
      @error = 'Failed to download all files'
    else
      block.call(1)
    end
  end

  def failed?
    !!@error
  end

  private

  # @return [Thread]
  def create_download_thread
    Thread.new do
      Thread.current[:progress] = 0
      @files_to_download = get_real_file_to_download
      @files_to_download.each_with_index do |filenames, index|
        download_file(filenames, index)
      end
      ScriptLoader.unpack_scripts if File.exist?(ScriptLoader::DEFLATE_SCRIPT_PATH)
    end
  end

  def get_real_file_to_download
    uri = URI(File.join(@game_url, @files_to_download.first))
    Net::HTTP.start(uri.host, uri.port, open_timeout: 2.0, use_ssl: uri.port != 80) do |http|
      http.request(Net::HTTP::Get.new(uri)) do |resp|
        return [] unless resp.code.to_i == 200

        return parse_index(resp.body)
      end
    end
  end

  # Parse the index
  # @param str [String] real index that should start with "PSDK_INDEX_DOWNLOAD\r\n"
  # @return [Array] if bad index, return empty array
  def parse_index(str)
    arr = str.split("\r\n")
    return [] if arr.shift != 'PSDK_INDEX_DOWNLOAD'

    return arr.map do |line|
      src, dest = line.split(':')
      dest.gsub!('%PSDK%', 'pokemonsdk')
      dest = 'pokemonsdk/version.txt' if src == 'version.txt' 
      next [src, dest]
    end
  end

  # @param src [String]
  # @param dest [String]
  # @param index [Integer]
  def download_file((src, dest), index)
    real_filename = File.expand_path(dest)
    file_url = URI(File.join(@game_url, @game_version, src))
    Net::HTTP.start(file_url.host, file_url.port, use_ssl: file_url.port != 80) do |http|
      request = Net::HTTP::Get.new(file_url)
      http.request(request) do |response|
        raise 'Failed to load file' if response.code.to_i != 200

        File.open(real_filename, 'wb') do |io|
          process_response(response, io, index)
        end
      end
    end
  end

  # @param response [Net::HTTPResponse]
  # @param io [File]
  # @param index [Integer]
  def process_response(response, io, index)
    total_size = (response['Content-Length'] || 1_048_576).to_i
    response.read_body do |chunk|
      io.write chunk
      current_pos = io.pos.clamp(0, total_size).to_f
      Thread.current[:progress] = (index + current_pos / total_size) / @files_to_download.size
    end
  end
end
