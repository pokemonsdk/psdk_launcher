puts "If you don't run as Administrator this command will not work"

# Safely create a directory
# @param path [String]
def mkdir(path)
  File.delete(path) if File.file?(path)
  Dir.mkdir(path) unless Dir.exist?(path)
end

dir = 'launcher_release'
mkdir(dir)
mkdir(File.join(dir, 'assets'))
IO.copy_stream('assets/calibri.ttf', File.join(dir, 'assets', 'calibri.ttf'))
%w[launcher LauncherHost.exe rubyw.exe msvcrt-ruby300.dll start.rb].each do |filename|
  IO.copy_stream(filename, File.join(dir, filename))
end

%w[games lib ruby_builtin_dlls].each do |dirname|
  system("CMD.EXE /C mklink /d #{dir}\\#{dirname} \"#{File.expand_path(dirname).gsub('/', '\\')}\"")
end

puts "Warning, the #{dir} folder is not able to be launched! You're supposed to compress it as zip with 7zip."
